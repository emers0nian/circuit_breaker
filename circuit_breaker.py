import time 

class CircuitBreaker:
  """Limit cascading failures by blocking requests when an error threshold is met across a time window."""

  CIRCUIT_CLOSED = 0
  CIRCUIT_OPEN = 1

  def __init__(self, http_client, error_threshold, time_window_in_seconds):
    self.http_client = http_client
    self.error_threshold = error_threshold
    self.time_window = time_window_in_seconds
    self.errors_at_epoch_second = {}
    self.state = self.CIRCUIT_CLOSED

  def do_request(self, url):
    if self.state == self.CIRCUIT_OPEN:
      if self.circuit_can_be_closed():
        self.close_circuit()
      else:
        raise CircuitBreakerError(self)

    try: 
      response = self.http_client.get(url)
    except http_client.exceptions.RequestException as e:
      self.handle_request_error(e)
    if response.status_code >= 400:
      self.handle_request_error(CircuitBreakerError(self, response))
    
    return response

  def handle_request_error(self, exception):
    self.increment_error_log()
    self.open_circuit_if_possible(exception)
    raise exception
  
  def increment_error_log(self):
    this_second = round(time.time())
    try: 
      self.errors_at_epoch_second[this_second] += 1
    except KeyError:
      self.errors_at_epoch_second[this_second] = 1
      self.purge_errors_outside_of_time_window(this_second)
  
  def purge_errors_outside_of_time_window(self, this_second):
    threshold = this_second - self.time_window
    keys_to_remove = []
    for epoch_second in self.errors_at_epoch_second.keys():
      if epoch_second < threshold:
        keys_to_remove.append(epoch_second)
    for key in keys_to_remove:
      self.errors_at_epoch_second.pop(key)

  def errors_within_time_window(self):
    return sum(self.errors_at_epoch_second.values())

  def open_circuit_if_possible(self, exception):
    self.purge_errors_outside_of_time_window(round(time.time()))
    if self.errors_within_time_window() >= self.error_threshold:
      self.state = self.CIRCUIT_OPEN
    raise exception

  def circuit_can_be_closed(self):
    self.purge_errors_outside_of_time_window(round(time.time()))
    if self.errors_within_time_window() < self.error_threshold:
      return True
    return False

  def close_circuit(self):
    self.state = self.CIRCUIT_CLOSED
    print(f'INFO - CircuitBreaker: Closed circuit, errors under threshold. ({self.state_string()})')

  def state_string(self):
    return f'{self.errors_within_time_window()} errors within {self.time_window}s, {len(self.errors_at_epoch_second)}s of error data in log'

class CircuitBreakerError(Exception):
  def __init__(self, circuit_breaker, http_response=None, message="CircuitBreaker is open"):
      self.circuit_breaker = circuit_breaker
      self.http_response = http_response
      self.message = message
      super().__init__(self.message)

  def __str__(self):
      if self.http_response:
        return f'WARNING - CircuitBreaker: HTTP error while circuit is closed. {self.http_response.status_code} - {self.http_response.request_method} {self.http_response.url} ({self.circuit_breaker.state_string()})'
      else:
        return f'ERROR - CircuitBreaker: Circuit open. Request blocked. ({self.circuit_breaker.state_string()})'

class MockedRequest:
  def __init__(self, mocked_response_status_code=200):
    self.mocked_response_status_code = mocked_response_status_code

  def get(self, url):
    class MockedResponse:
      def __init__(self, request_method, url, body_text, status_code):
        self.request_method = request_method
        self.url = url
        self.text = body_text
        self.status_code = status_code
    
    return MockedResponse('GET', url, '{"data": "abc"}', self.mocked_response_status_code)

if __name__ == "__main__":
  def do_mocked_request(url, status_code):
    try:
      breaker.http_client = MockedRequest(status_code)
      return breaker.do_request(url)
    except Exception as e:
      print(e)

  def request_success(breaker, url):
    return do_mocked_request(url, 200)

  def request_failure_500(breaker, url):
    return do_mocked_request(url, 500)

  test_url = "http://127.0.0.1:3000/testing"

  print("\n=== CircuitBreaker Test: Triggering at 5 errors across a 10 second time window\n")
  breaker = CircuitBreaker(MockedRequest(), 5, 10)

  response = request_success(breaker, test_url)
  print("INFO - Successful response: " + response.text)

  for _ in range(4):
    request_failure_500(breaker, test_url)
  for _ in range(10):
    request_success(breaker, test_url)
  print('INFO - Sleeping 8 seconds...')
  time.sleep(8)
  for _ in range(2):
    request_failure_500(breaker, test_url)
  for _ in range(5):
    time.sleep(1)
    request_failure_500(breaker, test_url)
  
  print("\n=== CircuitBreaker Test:Triggering at 2 errors across a 2 second time window\n")
  breaker = CircuitBreaker(MockedRequest(), 2, 2)

  response = request_success(breaker, test_url)
  print("INFO - Successful response: " + response.text)
  request_failure_500(breaker, test_url)
  request_failure_500(breaker, test_url)
  request_success(breaker, test_url)
  print('INFO - Sleeping 3 seconds...')
  time.sleep(3)
  request_failure_500(breaker, test_url)
  request_failure_500(breaker, test_url)
  request_success(breaker, test_url)
  time.sleep(1)
  request_failure_500(breaker, test_url)
  request_failure_500(breaker, test_url)
  request_success(breaker, test_url)
  time.sleep(3)
  request_failure_500(breaker, test_url)
