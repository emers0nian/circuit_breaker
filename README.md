# Circuit Breaker

Limit cascading failures by blocking requests when an error threshold is met across a time window.

## Installation

Ensure that you have Python 3 installed.

```
python3 circuit_breaker.py
```

## Potential Improvements

- Use a more efficient sliding window data structure for tracking errors per second
- Circuit breaker state is shared across the infrastructure (perhaps using Redis)
- A devops team member can manually open/close a circuit
- Safelist specific exceptions that should or should not open the circuit breaker
- Consider HTTP response time as a possible way to trigger the breaker
- Implement a "half open" circuit state

